package ru.t1.kharitonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.kharitonova.tm.enumerated.Status;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-complete-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(id, Status.COMPLETED);
        getProjectEndpoint().changeProjectStatusById(request);
    }

}
