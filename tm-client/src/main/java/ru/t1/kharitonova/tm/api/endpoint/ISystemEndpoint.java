package ru.t1.kharitonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.kharitonova.tm.dto.request.system.ServerVersionRequest;
import ru.t1.kharitonova.tm.dto.response.system.ServerAboutResponse;
import ru.t1.kharitonova.tm.dto.response.system.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
