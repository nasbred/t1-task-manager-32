package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.kharitonova.tm.model.Task;
import ru.t1.kharitonova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show tasks by project id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(projectId);
        @Nullable final List<Task> tasks = getTaskEndpoint().listTaskByProjectId(request).getTasks();
        renderTasks(tasks);
    }

}
