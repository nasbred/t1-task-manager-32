package ru.t1.kharitonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Create project.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(name, description);
        getProjectEndpoint().createProject(request);
    }

}
