package ru.t1.kharitonova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.domain.DataYamlSaveFasterXmlRequest;
import ru.t1.kharitonova.tm.enumerated.Role;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @Nullable
    public String getDescription() {
        return "Save data to yaml file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-yaml-faster-xml";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA SAVE YAML]");
        getDomainEndpoint().saveDataYamlFasterXml(new DataYamlSaveFasterXmlRequest());
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
