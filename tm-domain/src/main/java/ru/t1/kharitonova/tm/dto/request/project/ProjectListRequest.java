package ru.t1.kharitonova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractUserRequest;
import ru.t1.kharitonova.tm.enumerated.ProjectSort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private ProjectSort sort;

    public ProjectListRequest(@Nullable final ProjectSort sort) {
        this.sort = sort;
    }

}
