package ru.t1.kharitonova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractIndexRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskRemoveByIndexRequest extends AbstractIndexRequest {

    public TaskRemoveByIndexRequest(@Nullable final Integer index) {
        super(index);
    }

}
