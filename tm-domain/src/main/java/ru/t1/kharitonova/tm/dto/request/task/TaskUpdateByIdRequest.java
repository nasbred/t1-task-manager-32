package ru.t1.kharitonova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractIdRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIdRequest extends AbstractIdRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIdRequest(
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(id);
        this.name = name;
        this.description = description;
    }

}
