package ru.t1.kharitonova.tm.exception.user;

import ru.t1.kharitonova.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}
