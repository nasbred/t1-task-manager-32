package ru.t1.kharitonova.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kharitonova.tm.dto.request.AbstractIndexRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIndexRequest extends AbstractIndexRequest {

    public ProjectCompleteByIndexRequest(@Nullable final Integer index) {
        super(index);
    }

}
