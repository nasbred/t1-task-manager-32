package ru.t1.kharitonova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.endpoint.*;
import ru.t1.kharitonova.tm.api.repository.IProjectRepository;
import ru.t1.kharitonova.tm.api.repository.ITaskRepository;
import ru.t1.kharitonova.tm.api.repository.IUserRepository;
import ru.t1.kharitonova.tm.api.service.*;
import ru.t1.kharitonova.tm.dto.request.domain.*;
import ru.t1.kharitonova.tm.dto.request.project.*;
import ru.t1.kharitonova.tm.dto.request.system.ServerAboutRequest;
import ru.t1.kharitonova.tm.dto.request.system.ServerVersionRequest;
import ru.t1.kharitonova.tm.dto.request.task.*;
import ru.t1.kharitonova.tm.dto.request.user.*;
import ru.t1.kharitonova.tm.endpoint.*;
import ru.t1.kharitonova.tm.enumerated.Role;
import ru.t1.kharitonova.tm.model.User;
import ru.t1.kharitonova.tm.repository.ProjectRepository;
import ru.t1.kharitonova.tm.repository.TaskRepository;
import ru.t1.kharitonova.tm.repository.UserRepository;
import ru.t1.kharitonova.tm.service.*;
import ru.t1.kharitonova.tm.util.SystemUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final Server server = new Server(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);

        server.registry(DataBackupLoadRequest.class, domainEndpoint::loadDataBackup);
        server.registry(DataBackupSaveRequest.class, domainEndpoint::saveDataBackup);
        server.registry(DataBase64LoadRequest.class, domainEndpoint::loadDataBase64);
        server.registry(DataBase64SaveRequest.class, domainEndpoint::saveDataBase64);
        server.registry(DataBinaryLoadRequest.class, domainEndpoint::loadDataBinary);
        server.registry(DataBinarySaveRequest.class, domainEndpoint::saveDataBinary);
        server.registry(DataJsonLoadFasterXmlRequest.class, domainEndpoint::loadDataJsonFasterXml);
        server.registry(DataJsonSaveFasterXmlRequest.class, domainEndpoint::saveDataJsonFasterXml);
        server.registry(DataJsonLoadJaxbRequest.class, domainEndpoint::loadDataJsonJaxb);
        server.registry(DataJsonSaveJaxbRequest.class, domainEndpoint::saveDataJsonJaxb);
        server.registry(DataXmlLoadJaxbRequest.class, domainEndpoint::loadDataXmlJaxb);
        server.registry(DataXmlSaveJaxbRequest.class, domainEndpoint::saveDataXmlJaxb);
        server.registry(DataXmlLoadFasterXmlRequest.class, domainEndpoint::loadDataXmlFasterXml);
        server.registry(DataXmlSaveFasterXmlRequest.class, domainEndpoint::saveDataXmlFasterXml);
        server.registry(DataYamlLoadFasterXmlRequest.class, domainEndpoint::loadDataYamlFasterXml);

        server.registry(ProjectChangeStatusByIdRequest.class, projectEndpoint::changeProjectStatusById);
        server.registry(ProjectChangeStatusByIndexRequest.class, projectEndpoint::changeProjectStatusByIndex);
        server.registry(ProjectCreateRequest.class, projectEndpoint::createProject);
        server.registry(ProjectListRequest.class, projectEndpoint::listProject);
        server.registry(ProjectShowByIdRequest.class, projectEndpoint::showProjectById);
        server.registry(ProjectShowByIndexRequest.class, projectEndpoint::showProjectByIndex);
        server.registry(ProjectUpdateByIdRequest.class, projectEndpoint::updateProjectById);
        server.registry(ProjectUpdateByIndexRequest.class, projectEndpoint::updateProjectByIndex);

        server.registry(TaskChangeStatusByIdRequest.class, taskEndpoint::changeStatusById);
        server.registry(TaskChangeStatusByIndexRequest.class, taskEndpoint::changeStatusByIndex);
        server.registry(TaskClearRequest.class, taskEndpoint::clearTask);
        server.registry(TaskCreateRequest.class, taskEndpoint::createTask);
        server.registry(TaskListRequest.class, taskEndpoint::listTask);
        server.registry(TaskListByProjectIdRequest.class, taskEndpoint::listTaskByProjectId);
        server.registry(TaskShowByIdRequest.class, taskEndpoint::showTaskById);
        server.registry(TaskShowByIndexRequest.class, taskEndpoint::showTaskByIndex);
        server.registry(TaskRemoveByIdRequest.class, taskEndpoint::removeTaskById);
        server.registry(TaskRemoveByIndexRequest.class, taskEndpoint::removeTaskByIndex);
        server.registry(TaskUpdateByIdRequest.class, taskEndpoint::updateTaskById);
        server.registry(TaskUpdateByIndexRequest.class, taskEndpoint::updateTaskByIndex);
        server.registry(TaskBindToProjectRequest.class, taskEndpoint::bindTaskToProject);
        server.registry(TaskUnbindFromProjectRequest.class, taskEndpoint::unbindTaskToProject);

        server.registry(UserLockRequest.class, userEndpoint::lockUser);
        server.registry(UserUnlockRequest.class, userEndpoint::unlockUser);
        server.registry(UserRegistryRequest.class, userEndpoint::registryUser);
        server.registry(UserRemoveRequest.class, userEndpoint::removeUser);
        server.registry(UserUpdateProfileRequest.class, userEndpoint::updateUserProfile);
        server.registry(UserProfileRequest.class, userEndpoint::viewProfileUser);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull final User userTest = userService.create("test", "test", "test@test.ru");
        @NotNull final User userUser = userService.create("user", "user", "user@user.ru");
        @NotNull final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTest.getId(), "TEST PROJECT 2", "UserTest Project 2");
        projectService.create(userTest.getId(), "TEST PROJECT 1", "UserTest Project 1");
        projectService.create(userUser.getId(), "TEST PROJECT 3", "UserUser Project 3");
        projectService.create(userUser.getId(), "TEST PROJECT 4", "UserUser Project 4");
        projectService.create(userAdmin.getId(), "TEST PROJECT 5", "UserAdmin Project 5");
        projectService.create(userAdmin.getId(), "TEST PROJECT 6", "UserAdmin Project 6");

        taskService.create(userTest.getId(), "TEST TASK 1", "DESCRIPTION 1");
        taskService.create(userTest.getId(), "TEST TASK 2", "DESCRIPTION 2");
        taskService.create(userUser.getId(), "TEST TASK 3", "DESCRIPTION 3");
        taskService.create(userUser.getId(), "TEST TASK 4", "DESCRIPTION 4");
        taskService.create(userAdmin.getId(), "TEST TASK 5", "DESCRIPTION 5");
        taskService.create(userAdmin.getId(), "TEST TASK 6", "DESCRIPTION 6");
    }

    public void start() {
        initPID();
        initDemoData();
        loggerService.info("** WELCOME TO TASK MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
        server.start();
    }

    public void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        server.stop();
    }

}
