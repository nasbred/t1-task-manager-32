package ru.t1.kharitonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.api.endpoint.IDomainEndpoint;
import ru.t1.kharitonova.tm.api.service.IServiceLocator;
import ru.t1.kharitonova.tm.dto.request.domain.*;
import ru.t1.kharitonova.tm.dto.response.domain.*;
import ru.t1.kharitonova.tm.enumerated.Role;

public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @Override
    public @NotNull DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @Override
    public @NotNull DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @Override
    public @NotNull DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @Override
    public @NotNull DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @Override
    public @NotNull DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @Override
    public @NotNull DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonFasterXml();
        return new DataJsonLoadFasterXmlResponse();
    }

    @Override
    public @NotNull DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonFasterXml();
        return new DataJsonSaveFasterXmlResponse();
    }

    @Override
    public @NotNull DataJsonLoadJaxbResponse loadDataJsonJaxb(@NotNull final DataJsonLoadJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataJsonJaxb();
        return new DataJsonLoadJaxbResponse();
    }

    @Override
    public @NotNull DataJsonSaveJaxbResponse saveDataJsonJaxb(@NotNull final DataJsonSaveJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataJsonJaxb();
        return new DataJsonSaveJaxbResponse();
    }

    @Override
    public @NotNull DataXmlLoadJaxbResponse loadDataXmlJaxb(@NotNull final DataXmlLoadJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataXmlLoadJaxbResponse();
    }

    @Override
    public @NotNull DataXmlSaveJaxbResponse saveDataXmlJaxb(@NotNull final DataXmlSaveJaxbRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlJaxb();
        return new DataXmlSaveJaxbResponse();
    }

    @Override
    public @NotNull DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataXmlFasterXml();
        return new DataXmlLoadFasterXmlResponse();
    }

    @Override
    public @NotNull DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().saveDataXmlFasterXml();
        return new DataXmlSaveFasterXmlResponse();
    }

    @Override
    public @NotNull DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        check(request, Role.ADMIN);
        getServiceLocator().getDomainService().loadDataYamlFasterXml();
        return new DataYamlLoadFasterXmlResponse();
    }

}
